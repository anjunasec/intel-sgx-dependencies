SUDO=sudo
PACKAGE_INSTALL_CMD=apt-get install -y
INSTALL_CMD=${SUDO} ${PACKAGE_INSTALL_CMD}
PYTHON_INSTALL_CMD=${SUDO} -H pip install

.PHONY: all
all: pre-requisites install

.PHONY: pre-requisites
pre-requisites: submodules check-sgx check-linux-headers

.PHONY: get-dependent-packages
get-dependent-packages:
	${INSTALL_CMD} git
	${INSTALL_CMD} build-essential wget python
	${INSTALL_CMD} libssl-dev libcurl4-openssl-dev libprotobuf-dev libelf-dev
	${INSTALL_CMD} gawk
	${INSTALL_CMD} python-pip
	${PYTHON_INSTALL_CMD} google-cloud pycrypto

.PHONY: submodules
submodules:
	git submodule update --init

.PHONY: intel-build
intel-build: get-dependent-packages
	#sudo dependencies/ubuntu64-server/sgx_linux_x64_driver_dc5858a.bin
	#sudo dependencies/ubuntu64-server/sgx_linux_x64_psw_2.2.100.45311.bin
	#sudo dependencies/ubuntu64-server/sgx_linux_x64_sdk_2.2.100.45311.bin  --prefix /opt/intel
	sudo dependencies/ubuntu64-desktop/sgx_linux_x64_driver_dc5858a.bin
	sudo dependencies/ubuntu64-desktop/sgx_linux_x64_psw_2.2.100.45311.bin
	sudo dependencies/ubuntu64-desktop/sgx_linux_x64_sdk_2.2.100.45311.bin --prefix /opt/intel
	cd dependencies/gr-sgx && ISGX_DRIVER_PATH="../linux-sgx-driver" ISGX_DRIVER_VERSION="2.1" make
	# SGX_SDK=/opt/intel/sgxsdk/sgxsdk make -C dependencies/anjuna-utils/sgx-device-caps run

.PHONY: check-sgx
check-sgx:
	${INSTALL_CMD} build-essential
	cd dependencies/sgx-hardware && gcc test-sgx.c -o test-sgx && ./test-sgx

.PHONY: check-linux-headers
check-linux-headers:
	dpkg-query -s linux-headers-$(shell uname -r) || ${INSTALL_CMD} linux-headers-$(shell uname -r)

.PHONY: install
install: intel-build
	@bash -x ./install-drivers.sh
	sudo sysctl vm.mmap_min_addr=0
