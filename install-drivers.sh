#!/bin/bash

# stop all services
sudo service aesmd stop
sudo /sbin/rmmod graphene_sgx

sudo insmod ./dependencies/gr-sgx/graphene-sgx.ko
sudo service aesmd start    
