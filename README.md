# intel-sgx-dependencies

Introduction
------------
This repository provides an easy way to compile/install the Intel SGX software stack on a new machine.
The Intel SGX software stack depends on a number of packages:

- The Intel(R) SGX Driver: see [linux-sgx-driver](https://github.com/01org/linux-sgx-driver)
- The Intel(R) SGX SDK and Intel(R) SGX PSW Package: see [linux-sgx](https://github.com/intel/linux-sgx)

The currently shipping Intel binary packages do not always install properly on all versions of Ubuntu, so
compiling from source is often required. This repository provides a simple script to perform all the
operations to prepare a Linux host for running SGX applications.

Documentation
-------------

### Prerequisites:

1. The following Operating System are supported:
    - Ubuntu 16.04 LTS Desktop 64bits
    - Ubuntu 16.04 LTS Server 64bits
  
1. The following packages should be installed:
    - git
    - make
  
1. The proper permissions to install packages and kernel modules (required for the Intel(R) software stack)

Use the following command(s) to install the required tools to build the Intel(R) SGX SDK:

```
$ sudo apt-get update   
$ sudo apt-get install git make
```

### Getting this code
Clone this repository on the host that will be running SGX applications

### Running the script
In the root directory of this repository, run the following command:

```
$ make
```

Since the script will install Linux packages (tools required to compile the Intel(R) software stack),
you will be prompted to enter your administrator password (sudo).
